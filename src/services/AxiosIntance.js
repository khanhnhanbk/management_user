import axios from 'axios';

const url = 'http://localhost:15000';

const AxiosInstance = axios.create({
  baseURL: process.env.REACT_APP_BACKEND_URL || url,
  headers: {
    'content-type': 'application/json'
  }
});

AxiosInstance.interceptors.request.use((config) => {
  // const token = `Bearer ${localStorage.getItem('token')}`;
  // config.headers.common.token = token;
  // config.errorContext = new Error('Thrown at:'); // tracking error location
  return config;
});

AxiosInstance.interceptors.response.use(
  (response) => {
    if (response && response.data) {
      return response.data;
    }
    return response;
  },
  (error) => {
    // Handle errors
    throw error;
  }
);

export default AxiosInstance;
