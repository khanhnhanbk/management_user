import AxiosInstance from './AxiosIntance';

export const authService = {
  login: async (credentials) => {
    const body = {
      email: credentials.email,
      password: credentials.password
    };
    const res = await AxiosInstance.post('/api/v1/auth ', body);
    console.log('🚀 ~ file: authService.js:6 ~ login: ~ res:', res);
    return res;
  }
  // updateQuestion: async (question) => {
  //   const res = await AxiosInstance.put('/question', question);
  //   return res;
  // }
};
