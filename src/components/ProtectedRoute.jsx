import { useSelector } from 'react-redux';
import { Navigate } from 'react-router-dom';

export const ProtectedRoute = ({ children }) => {
  const user = useSelector((state) => state.auth.user);
  console.log('🚀 ~ file: ProtectedRoute.jsx:6 ~ ProtectedRoute ~ auth:', user);
  if (!user) {
    // user is not authenticated
    return <Navigate to="/login" />;
  }
  return children;
};
