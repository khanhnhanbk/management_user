// types
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { authService } from '../../services/index';
// initial state
const initialState = {
  user: null,
  token: null,
  isLoading: false,
  error: null
};

export const loginAsync = createAsyncThunk('auth/login', async (credentials, thunkAPI) => {
  try {
    const response = await authService.login(credentials);
    return response;
  } catch (error) {
    return thunkAPI.rejectWithValue(error.response.data);
  }
});
// ==============================|| SLICE - AUTH ||============================== //

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    logout: (state) => {
      state.user = null;
      state.token = null;
      state.isLoading = false;
      state.error = null;
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(loginAsync.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(loginAsync.fulfilled, (state, action) => {
        state.isLoading = false;
        console.log('🚀 ~ file: auth.js:39 ~ .addCase ~ action:', action);
        state.token = action.payload.token;
      })
      .addCase(loginAsync.rejected, (state, action) => {
        state.isLoading = false;
        console.log('🚀 ~ file: auth.js:44 ~ .addCase ~ action:', action);
        state.error = action.payload.message;
      });
  }
});

export default authSlice.reducer;

export const { logout } = authSlice.actions;
